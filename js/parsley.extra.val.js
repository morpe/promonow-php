window.ParsleyValidator.addValidator('phone',function(value, requirement) {
    if(value != "" &&  $.isNumeric(value)) return true;
    else if($(requirement).val() != "" && $.isNumeric($(requirement).val() ) ) return true;
    else return false;
},34)
.addMessage('it', 'phone', "Richiesto un campo numerico");

window.ParsleyValidator.addValidator('mono',function(value, requirement) {
	oc = $(requirement).is(':checked');
	if(oc && value != "") return true;
	else if (!oc && value == "") return true;
	else return false;
},34)
.addMessage('it', 'mono', "Valore richiesto");
    
window.ParsleyValidator.addValidator('codfisc',function(value, requirement) {
	var oc = $(requirement).is(':checked');
	console.log("oc",oc);
	if(oc) return isValidCodFisc16(value);
	else return true;
},35)
.addMessage('it', 'codfisc', "Formato codice fiscale errato");

window.ParsleyValidator.addValidator('optionpromo',function(value, requirements) {
      	if (requirements[1] == $(requirements[0]).val() && '' == value) {
      		return false;
      	}
      	else if(requirements[1] == $(requirements[0]).val() && $(requirements[2]).val() < value) {
      		return false;
      	}
      	else if(requirements[1] == $(requirements[0]).val() && !$.isNumeric(value)) {
      		return false;
      	}
      	else return true;
    },34)
.addMessage('it', 'optionpromo', "Questo campo non &egrave valido");

window.ParsleyValidator.addValidator('optionint',function(value, requirements) {
      	if (requirements[1] != $(requirements[0]).val() && '' == value) {
      		return false;
      	}
      /*	else if(requirements[1] != $(requirements[0]).val() && $(requirements[2]).val() < value) {
    		console.log("caso 12"); 
      		return false;
      	}*/
      	else if(requirements[1] != $(requirements[0]).val() && !$.isNumeric(value)) {
      		return false;
      	}
      	else return true;
    },34)
.addMessage('it', 'optionint', "Questo campo non &egrave valido");

window.ParsleyValidator.addValidator('mindate', function (value, requirement) {
    var d = $('body input[name=promoStart]').datepicker("getDate");
	startTimestamp = $.datepicker.formatDate('@', d);
	var nowTimestamp = $('body input[name=promoStartHidden]').val();
	return (startTimestamp > nowTimestamp);
}, 32)
.addMessage('it', 'mindate', 'La promozione non pu&ograve essere retroattiva');

window.ParsleyValidator.addValidator('promoend', function (value, requirement) {
	var d = $('body input[name=promoEnd]').datepicker("getDate");
	endTimestamp = $.datepicker.formatDate('@', d);
	var nowDate = new Date();
	$.datepicker.formatDate('@', nowDate);
	return (endTimestamp > nowDate);
}, 33);

window.ParsleyValidator.addValidator('promoendexp', function (value, requirement) {
	var d = $('body input[name=promoEnd]').datepicker("getDate");
	endTimestamp = $.datepicker.formatDate('@', d);
	var maxPromoValidityDays = maxExpire();
	var nowDate = new Date();
 	var dateEnd = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate()+maxPromoValidityDays,nowDate.getHours(),nowDate.getMinutes(),nowDate.getSeconds());
	expTimestamp = $.datepicker.formatDate('@', dateEnd);
	return (endTimestamp<expTimestamp);
	
}, 31)
.addMessage('it', 'promoendexp', 'inserisci una durata inferiore a %s giorni');

window.ParsleyValidator.addValidator('image',function(value, requirements) {
	console.log("ddddd"+$(requirements[1]).attr("src"));
	var imgpromo = $(requirements[1]).attr("src");
	if(typeof imgpromo != "undefined" && typeof $(requirements[0]).val() == "undefined") {
		console.log(5);
		return true;
	}
	else if(typeof imgpromo == "undefined" && typeof $(requirements[0]).val() != "undefined") {
		console.log(6);
		return true;
	}
	else return false;
},31)
.addMessage('it', 'image', "Questo campo non &egrave valido");

window.ParsleyValidator.addValidator('multimage',function(value, requirements) {
		var imgpromo = $(requirements[1]).attr("src");
    	if((typeof imgpromo != "undefined" && !imgpromo.includes("no-logo.png")) && $(requirements[0]).val() == "") {
    		console.log(1);
    		return true;
    	}
    	else if((typeof imgpromo == "undefined" || imgpromo.includes("no-logo.png")) && $(requirements[0]).val() != "") {
    		console.log(2);
    		return true;
    	}
    	else return false;
},34)
.addMessage('it', 'multimage', "Questo campo non &egrave valido");



function telCelSw(tel,cell){
	var telVal = $("#"+tel).val();
    var telLabel = $("#"+tel).data('label');
    var telLabTg = ".label"+tel;
   
    var cellVal = $("#"+cell).val();
    var cellLabel = $("#"+cell).data('label');
    var cellLabTg = ".label"+cell;
	var texT = $(telLabTg+" span.lab").text();
	var texM = $(cellLabTg+" span.lab").text();
   
    if(telVal == 0 && cellVal == 0) {             //se tel e cel non sono compilati             //- tel oblbigatorio
   	if(texT.indexOf("*") <= 0) $(telLabTg+" span.lab").text(texT+"(*)");
   	if(texM.indexOf("*") <= 0) $(cellLabTg+" span.lab").text(texM+"(*)");
    }
    else if(telVal != 0 && cellVal != 0) {            //se tel e cel sono compilati             //- tel oblbigatorio

    	if(texT.indexOf("*") < 0) $(telLabTg+" span.lab").text(texT+"(*)");
    	if(texM.indexOf("*") > 0 ) $(cellLabTg+" span.lab").text(texM.replace("(*)",""));
    }
    else if(telVal != 0 && cellVal == 0) {            //se tel ÃƒÂ¨ compilato e cell no            //- tel obbligatorio
      	if(texT.indexOf("*") < 0) $(telLabTg+" span.lab").text(texT+"(*)");
      	if(texM.indexOf("*") > 0 ) $(cellLabTg+" span.lab").text(texM.replace("(*)",""));
            
    }
    else if(telVal == 0 && cellVal != 0) {     //se cell non ÃƒÂ¨ compilato e tell no ÃƒÂ¨        //- cell obbligatorio
     	if(texM.indexOf("*") < 0) $(cellLabTg+" span.lab").text(texM+"(*)");
     	if(texT.indexOf("*") > 0 ) $(telLabTg+" span.lab").text(texT.replace("(*)",""));
    }
}
