function CC() {

	var cc = document.cookie.split("; ");
	var jsCC = {};
	$.each(cc, function(a, b) {
		var split = b.split("=");
		jsCC[split[0]] = decodeURIComponent(split[1]);
	});

	console.log(jsCC);
	
	this.defaults = {};
	this.defaults.expire = 1;

	this.set = function(cname, cvalue) {
		console.log("set",cname, cvalue);
		
		exdays = this.defaults.expire;
		
		var d = new Date();
		d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		var expires = "expires=" + d.toUTCString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	};

	this.get = function(tg) {
		console.log("get",tg);
		return jsCC[tg];
	};

	this.remove = function(name) {
		console.log("remove",name);
		document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	};

	this.clean = function() {
		console.log("clean");
		$.each(jsCC, function(a, b) {
			console.log("remove",a);
			document.cookie = a+"=; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
		});

	};

}