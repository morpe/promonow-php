	-- struttura --
	
	index.php carica:
		functions.php carica:
			config.php carica:
				pest/pestFunctions.php
				class/login.php';
				class/menu.php';
				class/page.php';
				
	-- config.php --
	
	contiene le informazioni per i vari ambienti di sviluppo, .dev, .php, .it e produzione
	wrappate in 3 funzioni dev(), test(), prod(),
	
	sono mappati anche i file dentro htmltpl tipo offline.php
	
	il tutto viene riversato nell'oggetto $ambient;
	
	-- pestFunctions.php --
	
	carica una libreria per usare i servizi rest
	pest - https://github.com/educoder/pest
	
	ho wrappato due chiamate in post e get nelle funzioni rGet() e rPost()
	queste, leggono da $ambient l'host per i servizi e la versione delle api
	
	rPost($serv,$data,$headers,$jwt=false)
	rGet($serv,$qs='',$jwt=false)
	
	$serv - url del servizio
	$data - array
	$headers - (non propriamente implementato)
	$jwt - per passare il token jwt direttamente nel $headers (non implementato)
	$qs - per passare le opzioni nel url, non e' icluso il marcatore '?'
	
	le funzioni rGet() e rPost() ogni volta che vengono invocate aggiungono 
	il risultato all oggetto $ambient e usano l'atributo $serv come idetificativono nel oggetto
	es: /categories/getAllCategories -> pest_categories_getAllCategories
	
	le funzioni rGet() e rPost() restituiscono un oggetto
	
------------------------

