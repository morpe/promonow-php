<?php

/*  chiamte ai servizi con pest  */
/* wrappate in funzioni */

require_once 'pest/Pest.php';
require_once 'pest/PestJSON.php';
require_once 'pest/PestXML.php';

function rGet($serv,$qs='',$jwt=false){
		
	if( $jwt ){
		//jwt
	}	

	global $ambient;
	$url = $ambient->host->server.$ambient->host->apiVersion;
	
	$pest = new Pest($url);
	$result = $pest->get($serv.$qs);
	
	$rr  = array();
	$rr = (object) $rr;
	$rr->PEST_GET = $pest;
	//$rr->JWT = $body->data->jwt;
	$rr->RESULT = json_decode($result);
	$rr->SERVICE = $serv;
	$rr->DOMAIN = $url;
	$rr->QUERY_STRINGS = $qs;
	
	$ambient->rGet['pest'.str_ireplace("/", "_", $serv)] = $rr;
	
	return $rr;
}

function rPost($serv,$data,$headers,$jwt=false){
		
	if( $jwt ){
		//jwt
	}		
	
	global $ambient;
	$url = $ambient->host->server.$ambient->host->apiVersion;
	
	$pest = new Pest($url);
	$result = $pest->post($serv, $data, $headers);

	$body = json_decode($pest->last_response['body']);

	$rr  = array();
	$rr = (object) $rr;
	$rr->PEST_POST = $pest;
	$rr->JWT = $body->data->jwt;
	$rr->RESULT = json_decode($result);
	$rr->SERVICE = $serv;
	$rr->DOMAIN = $url;
	$rr->DATA = $data;
	$rr->HEDERS = $headers;
	
	$ambient->rPost['pest'.str_ireplace("/", "_", $serv)] = $rr;

	return $rr;
	
}

?>