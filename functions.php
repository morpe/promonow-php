<?php
/* config */
require_once 'config.php';
config();
$master = new Page();
$master->forms = new Forms();

/* ------ */

/* genera il path assoluto dinamioco per linkare un file nel html tipo css, js */
// restituisce  una stringa
function get_linkFile($fileName = '') {
	global $ambient;

	$http = "http://";
	if ($ambient -> host -> https)
		$http = "https://";

	$furl = $http . $ambient -> server_name . "/" . $fileName . "?v=" . $ambient -> version;

	return $furl;
}

// stampa una stringa
function linkFile($fileName = '') {
	echo get_linkFile($fileName);
}

/* ------------------- */


function ancor($href = '', $label = '', $id = '', $class = '', $style='', $target = '' ,$js = '', $data = '') {
	//genera un tag <a><a>
	
	//$data = array();
	if( is_array($data) )
	{
		foreach ($data as $l => $v)
		{
			$d[] = "data-pn$l='$v'";
		}
	}
	
	$d = implode(" ",$d);
	
	$html ="<a ";
	$html .="href='$href'";
	$html .= "id='$id'";
	$html .= "class='$class'";
	$html .= "target='$target'";
	$html .= "style='$style'";
	$html .= $d;
	$html .= $js;
	$html .=">".$label."</a>";
	
	echo $html;
};


?>