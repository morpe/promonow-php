<?php 
/*classe per gestire il render delle pagine
eredita info da login e menu
usa i file presenti in htmltpl*/

class Page extends Menu {

	public $offline;
	public $pages;
	public $forms;
	public $htmlTplPath;
	public $head;
	public $htmlBuffer;
	
	public function __construct() {
		parent::__construct();
		ob_start();
		
		$this->listMethods = get_class_methods("Page");
		$this->info = "struttura: master<-menu<-page";
		$this->htmlTplPath = $this->A->root."/htmltpl";
		
		$this->offline = $this->A->offline;

		$this->loadHtmlTpl();
		
	}
	
	public function loadHtmlTpl(){
		$ls = scandir($this->htmlTplPath);
		$files = array();
		
		foreach ($ls as $i => $f)
		{
			$label = explode(".",$f);
			$label = $label[0];	
				
			if( stristr($f, ".php") )
			{
				$files[ $label ] = $f;
			}
		}
		
		$files = (object) $files;
		$this->pages = (object) $files;
		
		//var_dump($ls,$files);
	}
	
	public function debug(){
		$xdebugTrue = "<style>.xdebug-var-dump{display:block}</style>\n";
		$xdebugFalse = "<style>.xdebug-var-dump{display:none}</style>\n";
		//$xdebug = "<style>.xdebug-var-dump{background:#eee}</style>\n";

		if( isset($_GET['debug']) and $_GET['debug'] == "true")
		{
			$this->A->debug = TRUE;
		}
		
		if( isset($_GET['debug']) and $_GET['debug'] == "false")
		{
			$this->A->debug = FALSE;
		}
		
		if($this->A->debug)
		{
			echo $xdebugTrue;
		}
		else{
			echo $xdebugFalse;
		}
		
		
	}

	public function Head(){
		$this->debug();
			
		$head = $this->htmlTplPath."/".$this->pages->head;	
		include $head;
		echo "\n$this->head\n";
	}
	
	public function Body(){
		$body = $this->htmlTplPath."/".$this->pages->body;
		include $body;
	}
	
	public function Footer(){
		$footer = $this->htmlTplPath."/".$this->pages->footer;
		include $footer;
	}
	
	public function Offline()
	{
		$offline = $this->htmlTplPath."/offline.html";
		return file_get_contents($offline);
	}
	
	public function Render(){
			
		$this->htmlBuffer = ob_get_contents();	
		ob_end_clean();	

		if( $this->A->offline == false ){

			echo $this->htmlBuffer;
		}
		else
		{
			ob_end_clean();
			echo $this->Offline();
		}
	}
	
}

 ?>



