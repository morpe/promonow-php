<?php
/*classe per gestire le form
 eredita info da login.php*/
class Forms {
		
	public $info;
	public $listMethods;

	public function __construct() {
		$this->info = "forms";
		$this->listMethods = get_class_methods("Forms");
	}
	
	public function loginForm(){
		return "form login";
	}
	
	public function userProfile(){
		return "form user profile";
	}
	
	public function regShop(){
		return "form registra negozio";
	}
	
	public function regCostumer(){
		return "form registra cliente";
	}
	
	public function editShop(){
		return "form modifica negozo";
	}
	
	public function newPos(){
		return "form nuovo punto vendita";
	}
	
	public function editPos(){
		return "form modifica punto vendita";
	}
	
	public function newPromo(){
		return "form nuova promozione";
	}

}




?>